package com.Hotel.ApiEndpoint;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient

public class ApiEndpointApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiEndpointApplication.class, args);
	}

}
