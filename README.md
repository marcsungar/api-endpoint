# API Endpoint

Configure Front End requests to one general Back End

## *WHAT IS A MICROSERVICE*
*Microservices is an **architectural design** for building a distributed application **using containers**. They get their name because each function of the application **operates as an independent service**. This architecture **allows for each service to scale or update without disrupting other services** in the application. A microservices framework creates a massively **scalable and distributed system***

![Microservices Architecture](https://avinetworks.com/wp-content/uploads/2018/06/Microservices-vs-monolithic-architecture-diagram.png)
